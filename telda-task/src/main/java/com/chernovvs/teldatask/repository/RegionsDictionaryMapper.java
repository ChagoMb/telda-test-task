package com.chernovvs.teldatask.repository;

import com.chernovvs.teldatask.model.RegionsDictionary;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * Репозиторий для работы со справочником регионов
 */
@Mapper
public interface RegionsDictionaryMapper {

    List<RegionsDictionary> findAll();

    RegionsDictionary findById(Long id);

    void update(Long id,
                String name,
                String shortName);
}

