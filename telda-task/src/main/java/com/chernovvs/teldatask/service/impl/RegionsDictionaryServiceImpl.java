package com.chernovvs.teldatask.service.impl;

import com.chernovvs.teldatask.model.RegionsDictionary;
import com.chernovvs.teldatask.repository.RegionsDictionaryMapper;
import com.chernovvs.teldatask.service.RegionsDictionaryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Optional.ofNullable;

/**
 * Сервис для работы со справочником регионов
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RegionsDictionaryServiceImpl implements RegionsDictionaryService {

    private final RegionsDictionaryMapper regionsDictionaryMapper;

    /**
     * Получить все регионы
     *
     * @return Регионы
     */
    @Override
    @Transactional(readOnly = true)
    public List<RegionsDictionary> findAll() {
        log.info("Получаем все регионы из справочника");

        return regionsDictionaryMapper.findAll();
    }

    /**
     * Получить регион по идентификатору
     *
     * @param id Идентификатор
     * @return Регион
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<RegionsDictionary> findById(@NonNull Long id) {
        log.info("Получение региона с id = {}", id);

        return ofNullable(regionsDictionaryMapper.findById(id));
    }

    /**
     * Редактировать информацию о регионе
     *
     * @param regionsDictionary Редактированная информация
     */
    @Override
    @Transactional
    public void update(@NonNull RegionsDictionary regionsDictionary) {
        log.info("Обновление региона с id = {}", regionsDictionary.getId());

        regionsDictionaryMapper.update(regionsDictionary.getId(),
                regionsDictionary.getName(),
                regionsDictionary.getShortName());
    }
}
