package com.chernovvs.teldatask.service;

import com.chernovvs.teldatask.model.RegionsDictionary;
import org.springframework.lang.NonNull;

import java.util.List;
import java.util.Optional;

/**
 * Сервис для работы со справочником регионов
 */
public interface RegionsDictionaryService {

    /**
     * Получить все регионы
     *
     * @return Регионы
     */
    List<RegionsDictionary> findAll();

    /**
     * Получить регион по идентификатору
     *
     * @param id Идентификатор
     * @return Регион
     */
    Optional<RegionsDictionary> findById(@NonNull Long id);

    /**
     * Редактировать информацию о регионе
     *
     * @param regionsDictionary Редактированная информация
     */
    void update(@NonNull RegionsDictionary regionsDictionary);
}
