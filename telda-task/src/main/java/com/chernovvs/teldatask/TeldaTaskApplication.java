package com.chernovvs.teldatask;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@MapperScan(value = {"com.chernovvs.teldatask.repository"})
@SpringBootApplication
public class TeldaTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeldaTaskApplication.class, args);
	}

}
