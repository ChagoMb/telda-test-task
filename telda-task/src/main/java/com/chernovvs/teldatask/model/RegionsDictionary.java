package com.chernovvs.teldatask.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Справочник регионов
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RegionsDictionary {

    /**
     * Идентификатор
     */
    private Long id;

    /**
     * Наименование
     */
    private String name;

    /**
     * Короткое наименование
     */
    private String shortName;
}

