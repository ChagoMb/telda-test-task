package com.chernovvs.teldatask.controller;

import com.chernovvs.teldatask.model.RegionsDictionary;
import com.chernovvs.teldatask.service.RegionsDictionaryService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Api для работы со справочником регионов
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("api/dictionary")
public class RegionsDictionaryController {

    private final RegionsDictionaryService regionsDictionaryService;

    /**
     * Получить все регионы в справочнике
     *
     * @return Регионы
     */
    @GetMapping
    public ResponseEntity<List<RegionsDictionary>> findAll() {
        return ResponseEntity.ok(regionsDictionaryService.findAll());
    }

    /**
     * Получить регион по идентификатору
     *
     * @param id Идентификатор региона
     * @return Регион
     */
    @GetMapping("{id}")
    public ResponseEntity<RegionsDictionary> findById(@PathVariable Long id) {
        return regionsDictionaryService.findById(id)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }

    /**
     * Редактирование информации о регионе
     *
     * @param regionsDictionary Редактированная информация о регионе
     * @return Редакированный регион
     */
    @PutMapping
    public ResponseEntity<RegionsDictionary> update(@RequestBody RegionsDictionary regionsDictionary){
        regionsDictionaryService.update(regionsDictionary);

        return regionsDictionaryService.findById(regionsDictionary.getId())
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.noContent().build());
    }
}
