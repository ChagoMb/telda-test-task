package com.chernovvs.teldatask.service;

import com.chernovvs.teldatask.TeldaTaskApplicationTests;
import com.chernovvs.teldatask.model.RegionsDictionary;
import com.chernovvs.teldatask.repository.RegionsDictionaryMapper;
import com.chernovvs.teldatask.service.impl.RegionsDictionaryServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/**
 * Тестирование сервсиса работы со справочником регионов
 */
@Slf4j
public class RegionDictionaryServiceTest extends TeldaTaskApplicationTests {

    private final RegionsDictionaryMapper mapper = mock(RegionsDictionaryMapper.class);

    private final RegionsDictionaryService service = new RegionsDictionaryServiceImpl(mapper);

    /**
     * Тестирование поиска всех регионов
     */
    @Test
    public void findAll() {
        RegionsDictionary region = RegionsDictionary.builder()
                .id(1L)
                .name("Белгородская область")
                .shortName("1")
                .build();
        List<RegionsDictionary> expected = List.of(region);

        when(mapper.findAll()).thenReturn(expected);

        List<RegionsDictionary> result = service.findAll();

        verify(mapper).findAll();
        verifyNoMoreInteractions(mapper);

        assertThat(result).hasSize(1);
        assertThat(result).isEqualTo(expected);
    }

    /**
     * Тестирование поиска региона по идентификатору
     */
    @Test
    public void findById() {
        RegionsDictionary expected = RegionsDictionary.builder()
                .id(1L)
                .name("Белгородская область")
                .shortName("1")
                .build();

        when(mapper.findById(anyLong())).thenReturn(expected);

        Optional<RegionsDictionary> result = service.findById(1L);

        verify(mapper).findById(1L);
        verifyNoMoreInteractions(mapper);

        assertThat(result).isEqualTo(of(expected));
    }

    /**
     * Тестирование редактирования региона
     */
    @Test
    public void update() {
        RegionsDictionary expected = RegionsDictionary.builder()
                .id(1L)
                .name("Ханты-Мансийский Автономный огруг")
                .shortName("ХМАО")
                .build();

        doNothing().when(mapper).update(anyLong(), anyString(), anyString());

        service.update(expected);

        verify(mapper).update(1L, "Ханты-Мансийский Автономный огруг", "ХМАО");
        verifyNoMoreInteractions(mapper);
    }
}
